#!/usr/bin/env php
<?php


set_time_limit(0);
date_default_timezone_set('Africa/Johannesburg');

// include the composer autoloader
require_once __DIR__ . '/vendor/autoload.php';

// import the Symfony Console Application
use Symfony\Component\Console\Application;
use t4g\Build\Commands\CreatePHAR;

$app = new Application();
$app->add(new CreatePHAR());
$app->run();
?>