<?php
/**
 * Created by PhpStorm.
 * User: soap
 * Date: 14-10-23
 * Time: 6:16 AM
 */


namespace t4g\Build\Commands;

use Composer\Composer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;

class CreatePHAR extends Command {


    protected static $flags= [
        ['filename', 'o', InputOption::VALUE_REQUIRED, 'Specify the name of your new PHAR.' ],
        ['targetpath', 'd', InputOption::VALUE_OPTIONAL, 'Specify the target directory of you PHAR.' ],
    ];

    protected function configure()
    {
        $options = array();
        foreach( self::$flags as $flag){
            $options[] = new InputOption($flag[0],$flag[1],$flag[2],$flag[3],$flag[2]===InputOption::VALUE_REQUIRED ? Null : $flag[0]);
        }
        print_r($options);


        $this->setName("gen")
            ->setDescription("Generate a new PHAR archive.")
            ->setDefinition($options
              )
            ->setHelp(<<<EOT
Create executable PHAR from .snubs

Usage:

If you don't specify options it will be taken from the JSON file at the root of opphar
<info>opphar gen -ocheese  <env></info>

EOT
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $header_style = new OutputFormatterStyle('white', 'green', array('bold'));
        $output->getFormatter()->setStyle('header', $header_style);

        $start = intval($input->getOption('start'));
        $stop  = intval($input->getOption('stop'));

        if ( ($start >= $stop) || ($start < 0) ) {
            throw new \InvalidArgumentException('l');
        }

        $output->writeln('<header>Loading files</header>');

        $xnM2 = 0; // set x(n-2)
        $xnM1 = 1;  // set x(n-1)
        $xn = 0; // set x(n)
        $totalFiboNr = 0;
        while ($xnM2 <= $stop)
        {
            if($xnM2 >= $start)  {
                $output->writeln('<header>'.$xnM2.'</header>');
                $totalFiboNr++;
            }
            $xn = $xnM1 + $xnM2;
            $xnM2 = $xnM1;
            $xnM1 = $xn;

        }
        $output->writeln('<header>Total found = '.$totalFiboNr.' </header>');
    }
}